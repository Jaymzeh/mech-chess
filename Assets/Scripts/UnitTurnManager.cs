﻿using UnityEngine;
using System.Collections.Generic;

/*
This class stores all the data that is most used during combat, such as organising which unit is being controlled
and storing the data that which tiles units can move to and attack.
*/
public class UnitTurnManager : MonoBehaviour {

    static UnitTurnManager instance;
    public static Unit ControlledUnit { get; set; }
    public static Unit TargettedUnit { get; set; }
    public static List<Unit> units = new List<Unit>();
    int unitIndex = -1;
    static int playerUnitsAlive, enemyUnitsAlive;
    
    public enum TurnState { Idle = 0, Moving = 1, Action = 2}
    public static TurnState state = TurnState.Idle;

    public GameObject tilePrefab;
    List<GameObject> tiles = new List<GameObject>();
    public Material tileMatWhite, tileMatBlue, tileMatRed;
    public static HashSet<Vector3> MoveTiles { get; private set; }
    public static HashSet<Vector3> AttackTiles { get; private set; }

    void Awake() {
        instance = this;
        GameController.InputEnabled = true;
        Grid = GameObject.FindGameObjectWithTag("Grid").GetComponent<Grid>();
        GameObject[] unitsInScene = GameObject.FindGameObjectsWithTag("Unit");
        for (int i = 0; i < unitsInScene.Length; i++) {
            units.Add(unitsInScene[i].GetComponent<Unit>());
        }
        units = SortList();
        StartTurn();
    }//Awake

    //void Start() {
    //    Grid = GameObject.FindGameObjectWithTag("Grid").GetComponent<Grid>();
    //    GameObject[] unitsInScene = GameObject.FindGameObjectsWithTag("Unit");
    //    for (int i = 0; i < unitsInScene.Length; i++) {
    //        units.Add(unitsInScene[i].GetComponent<Unit>());
    //    }
    //    units = SortList();
    //    StartTurn();
    //}//Start

    /*
    Sort the List<Unit> storing the units in the level. This function orders the player's units
    before the enemy units to allow for easy traversal through the list
    */
    List<Unit> SortList() {
        List<Unit> newList = new List<Unit>();
        List<Unit> temp = new List<Unit>();

        for (int i = 0; i < units.Count; i++) {

            if (units[i].team == 0) {
                newList.Add(units[i]);
                playerUnitsAlive++;
            }
            else {
                temp.Add(units[i]);
                enemyUnitsAlive++;
            }
        }
        for (int i = 0; i < temp.Count; i++) {
            newList.Add(temp[i]);
        }
        print("Player units["+ playerUnitsAlive + "]");
        print("Enemy units["+ enemyUnitsAlive + "]");
        return newList;
    }//SortList

    public static Grid Grid { get; private set; }//Grid

    public static void SetState(TurnState newState) {
        state = newState;

        switch (state) {
            case TurnState.Moving:
                GetMoveTiles();
                break;
            case TurnState.Action:
                GetAttackTiles(ControlledUnit.weapon);
                break;
            case TurnState.Idle:

                break;
        }
    }

    /*
    Determine which unit should be controlled next, and display new turn screen if the
    opposite team's turn has started.
    */
    public static void StartTurn() {
        instance.unitIndex++;
        if (instance.unitIndex > units.Count - 1) {
            instance.unitIndex = 0;
            //ShowPlayerTurn.DisplayTurnChange(true);
        }
        //if(instance.unitIndex==instance.playerUnitCount)
            //ShowPlayerTurn.DisplayTurnChange(false);
        ControlledUnit = units[instance.unitIndex];
        Grid.UpdateGrid();
        state = TurnState.Moving;
        ControlledUnit.StartTurn();
    }//StartTurn

    /*
    Store the tiles that the current unit is able to move to based on their available
    action points.
    */
    public static void GetMoveTiles() {
        ClearMoveTiles();
        MoveTiles = new HashSet<Vector3>();
        foreach (Node n in Grid.Nodes) {
            if (n.worldPosition != ControlledUnit.transform.position) {
                float range = ControlledUnit.actionPoints * (Grid.nodeRadius*2) + Grid.nodeRadius;

                if (Vector3.Distance(ControlledUnit.transform.position, n.worldPosition) <= (range)) {
                    PathRequestManager.RequestPath(ControlledUnit.transform.position, n.worldPosition, instance.TilePathFound, true);
                }
            }
        }
    }//GetAvailableTiles

    /*
    Add new tiles to the list of available move tiles if there is an available path.
    */
    void TilePathFound(Vector3[] newPath, bool pathSuccess) {
        if (pathSuccess && (newPath.Length) <= ControlledUnit.actionPoints) {
            for (int i = 0; i < newPath.Length; i++)
                if (!MoveTiles.Contains(newPath[i])) {
                    MoveTiles.Add(newPath[i]);
                    GameObject newTile = (GameObject)Instantiate(tilePrefab, newPath[i], Quaternion.identity);
                    newTile.transform.localScale = new Vector3(Grid.nodeRadius, 1, Grid.nodeRadius);
                    newTile.GetComponent<Renderer>().material = tileMatBlue;
                    newTile.transform.parent = Grid.transform.GetChild(0);
                    tiles.Add(newTile);
                }
            //int i = newPath.Length-1;
            //MoveTiles.Add(newPath[i]);
            //GameObject newTile = (GameObject)Instantiate(tilePrefab, newPath[i], Quaternion.identity);
            //newTile.transform.localScale = new Vector3(Grid.nodeRadius, 1, Grid.nodeRadius);
            //newTile.GetComponent<Renderer>().material = tileMatBlue;
            //newTile.transform.parent = Grid.transform.GetChild(0);
            //tiles.Add(newTile);

        }
    }//TilePathFound

    public void Attack() {
        ControlledUnit.Attack(TargettedUnit);
    }//Attack

    /*
    Store the tiles that the currently used weapon can attack in.
    */
    public static void GetAttackTiles(Weapon weapon) {
        AttackTiles = new HashSet<Vector3>();
        float maxRange = weapon.maxRange * (Grid.nodeRadius*2) + Grid.nodeRadius;
        float minRange = weapon.minRange * (Grid.nodeRadius*2) + Grid.nodeRadius;

        foreach(GameObject t in instance.tiles)
            Destroy(t);
        instance.tiles = new List<GameObject>();

        foreach (Node n in Grid.Nodes) {
            if (n.worldPosition != ControlledUnit.transform.position) {
                if (Vector3.Distance(weapon.transform.position, n.worldPosition) <= maxRange &&
                    Vector3.Distance(weapon.transform.position, n.worldPosition) >= minRange &&
                    n.walkable) {

                    //Vector3 direction = (n.worldPosition - ControlledUnit.Viewpoint).normalized;
                    //Ray ray = new Ray(ControlledUnit.Viewpoint, direction);
                    //RaycastHit hit;
                    //if (Physics.Raycast(ray, out hit)) {
                    //    if (hit.point != n.worldPosition)
                    //        continue;
                    //}

                    AttackTiles.Add(n.worldPosition);
                    GameObject newTile = (GameObject)Instantiate(instance.tilePrefab, n.worldPosition, Quaternion.identity);
                    newTile.transform.localScale = new Vector3(Grid.nodeRadius, Grid.nodeRadius, Grid.nodeRadius);
                    if (n.occupied)
                        newTile.GetComponent<Renderer>().material = instance.tileMatWhite;
                    else
                        newTile.GetComponent<Renderer>().material = instance.tileMatRed;
                    newTile.transform.parent = Grid.transform.GetChild(0);
                    instance.tiles.Add(newTile);
                }
            }
        }
    }//GetAttackTiles

    public static void ClearMoveTiles() {
        MoveTiles = new HashSet<Vector3>();
        foreach(GameObject t in instance.tiles)
            Destroy(t);
        instance.tiles = new List<GameObject>();
    }//ClearAvailableTiles

    public static void ClearAttackTiles() {
        AttackTiles = new HashSet<Vector3>();
        foreach(GameObject t in instance.tiles)
            Destroy(t);
        instance.tiles = new List<GameObject>();
    }//ClearAvailableTiles

    /*
    Move the controlled unit back to the position it was at when the turn started.
    */
    public void MoveBack() {
        Grid.NodeFromWorldPoint(ControlledUnit.transform.position).occupied=false;
        ControlledUnit.MoveToStartPosition();
        SetState(TurnState.Moving);
    }

    /*
    End the turn of the currently controlled unit, and allow the next unit in the list to start
    its turn.
    */
    public static void EndTurn() {
        ClearMoveTiles();
        ClearAttackTiles();
        TargettedUnit = null;
        
        for(int i=0; i < units.Count; i++) {
            if (units[i].health <= 0) {
                if (units[i].team == 0)
                    playerUnitsAlive--;
                else
                    enemyUnitsAlive--;

                Destroy(units[i].gameObject);
                units.RemoveAt(i);
            }
        }
        if (playerUnitsAlive <= 0)
            print("GAME OVER - PLAYER LOSES");
        
        if (enemyUnitsAlive <= 0)
            print("GAME OVER - PLAYER WINS");
        Grid.UpdateGrid();
        StartTurn();
    }//EndTurn

    public void OnDrawGizmos() {
        //if (openAttackTiles != null) {
        //    Gizmos.color = Color.red;
        //    foreach (Vector3 n in openAttackTiles)
        //        Gizmos.DrawCube(n, Vector3.one * .7f);
        //}

        //if (openMoveTiles != null) {
        //    Gizmos.color = Color.blue;
        //    foreach (Vector3 n in openMoveTiles)
        //        Gizmos.DrawCube(n, Vector3.one * .6f);
        //}
    }//OnDrawGizmos
}
