﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    /*
    Global boolean that should be used to determine if the player is able to control the game.
    Example: disable input while cutscene is playing, or when enemy AI team is being controlled.
    */
    public static bool InputEnabled{
        get; set;
    }//InputEnabled

    /*
    Load a scene using the name of the scene using the LoadingScreen class.
    Warning: Scene must be added to Build Settings in Unity Editor in order to load the scene at runtime.
    */
    public static void LoadScene(string scene){
        LoadingScreen.loadingScreen.ChangeScene(scene);
    }//LoadScene
    
    /*
    Quits the game.
    */
    public static void ExitApplication(){
        Application.Quit();
    }//ExitApplication
}
