﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/*
Editor for Turn Change screen to allow for easy editing of banner colours and scroll speed.
*/
[CustomEditor(typeof(ShowPlayerTurn))]
public class TurnScreenEditor : Editor {

    public override void OnInspectorGUI(){

        //base.OnInspectorGUI();

        ShowPlayerTurn screen = (ShowPlayerTurn)target;

        screen.team = EditorGUILayout.IntField("Team", screen.team);

        EditorGUILayout.FloatField("Scroll speed ", screen.scrollSpeed);

        screen.teamColours[0] = EditorGUILayout.ColorField("Player Main Bar Colour", screen.teamColours[0]);
        screen.teamColours[1] = EditorGUILayout.ColorField("Enemy Main Bar Colour", screen.teamColours[1]);

        screen.teamColours[2] = EditorGUILayout.ColorField("Player Secondary Bar Colour", screen.teamColours[2]);
        screen.teamColours[3] = EditorGUILayout.ColorField("Enemy Secondary Bar Colour", screen.teamColours[3]);
    }
}
