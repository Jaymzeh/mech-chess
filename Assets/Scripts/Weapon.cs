﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
    Unit unit;
    Unit target;
    public int damage = 10;
    public int minRange = 1;
    public int maxRange = 3;
    public int accuracy = 50;



    void Start(){
        unit = GetComponent<Unit>();
    }
   
    /*
    Determines if the controlling unit has a line of sight on the target,
    and if the target is within range for attacking.
    */
    public bool CanSeeTarget() {
        UnitTurnManager.GetAttackTiles(this);
        Vector3 direction = target.transform.position - unit.transform.position;
        Ray ray = new Ray(unit.transform.position, direction);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit)) {
            if (hit.collider.name != target.name) {
                print("No line of sight on target [" + hit.collider.name + "]");
                return false;
            }
            else {
                //print("Extablished line of sight on target [" + hit.collider.name + "]");

                float _maxRange = maxRange * (UnitTurnManager.Grid.nodeRadius*2) + UnitTurnManager.Grid.nodeRadius;
                float _minRange = minRange * (UnitTurnManager.Grid.nodeRadius*2) + UnitTurnManager.Grid.nodeRadius;

                //print("Weapon Max Range [" + _maxRange + "]");
                //print("Weapon Min Range [" + _minRange + "]");

                float distance = Vector3.Distance(unit.transform.position, target.transform.position);
                //print("Distance to target [" + distance + "]");

                if (distance <= _maxRange && distance >= _minRange) {
                    //print("Target [" + hit.collider.name + "] within firing range");
                    return true;
                }
                //else
                    //print("Target [" + hit.collider.name + "] not within firing range");
            }
        }
        return false;

        
    }
    
    /*
    After determining if the target is able to be attacked, attack the target.
    If the accuracy if higher than the random number generated, the target will be hit.
    */
    public void Attack(Unit _target) {
        target = _target;
        if (CanSeeTarget()) {
            //print("Unit [" + unit.name + "] firing at target unit [" + target.name + "]");
            int hit = Random.Range(0, 100);
            if (hit <= accuracy) {
                print("Target HIT. Damage done[" + damage + "].");
                target.health -= damage;
            }
            else {
                print("Target MISSED. No damage taken by target.");
            }
        }
    }
}
