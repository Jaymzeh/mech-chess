﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour {

    public float moveSpeed = 20;
    Vector3[] path;
    int targetIndex;
    List<Vector3> availableTiles = new List<Vector3>();
    public int actionPoints, baseActionPoints;

    public int health = 100;
    public Weapon weapon;
    
    Animator anim;
    Vector3 startPos;
    Quaternion startRot;

    public enum Team { Player=0, Enemy = 1};
    public Team team;

    public Vector3 Viewpoint { get;private set;}

    void Start() {
        
        anim = GetComponent<Animator>();
    }

    /*
    Reset unit's action points to its original size, get movement tiles from
    UnitTurnManager class.
    */
    public void StartTurn() {
        startPos = transform.position;
        startRot = transform.rotation;
        actionPoints = baseActionPoints;
        Viewpoint = transform.position + transform.up;
        UnitTurnManager.GetMoveTiles();
    }//StartTurn

    /*
    End unit's turn and tell UnitTurnManager to start the next unit's turn.
    */
    public void EndTurn() {
        UnitTurnManager.EndTurn();
    }//EndTurn

    /*
    If the position passed into the function is one of the pre-determined tiles that the unit can move to,
    request a path to the tile.
    */
    public void Move(Vector3 newPos) {
        if (UnitTurnManager.MoveTiles.Contains(newPos))
            PathRequestManager.RequestPath(transform.position, newPos, OnPathFound, true);
    }//Move

    public Vector3[] Path {
        get {
            return path;
        }
        set {
            path = value;
        }
    }//Path

    /*
    If the PathRequestManager was able to find a path, make the unit follow the path and remove the action points
    needed to follow the path.
    */
    void OnPathFound(Vector3[] newPath, bool pathSuccess) {
        if (pathSuccess) {
            path = newPath;
            actionPoints -= path.Length;
            StartCoroutine("FollowPath");
        }
    }//OnPathFound

    /*
    Follow the path created by the PathRequestManager.
    */
    IEnumerator FollowPath() {
        anim.SetBool("Walking", true);
        Vector3 currentWayPoint = path[0];
        targetIndex = 0;
        while (true) {
            transform.LookAt(new Vector3(currentWayPoint.x,transform.position.y, currentWayPoint.z));
            if (transform.position == currentWayPoint) {
                targetIndex++;
                
                if (targetIndex >= path.Length) {
                    path = new Vector3[0];
                    UnitTurnManager.Grid.UpdateGrid();
                    UnitTurnManager.SetState(UnitTurnManager.TurnState.Action);
                    anim.SetBool("Walking", false);
                    Viewpoint = transform.position + transform.up;
                    yield break;
                }
                currentWayPoint = path[targetIndex];
                transform.LookAt(currentWayPoint);
            }
            transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, moveSpeed * Time.deltaTime);
            yield return null;
        }
    }//FollowPath

    public void MoveToStartPosition() {

        transform.position = startPos;
        transform.rotation = startRot;
        
        StartTurn();
    }

    /*
    Attack another unit using the currently equipped weapon.
    NOTE: Only one weapon available at time of writing. More weapon variety planned.
    */
    public void Attack(Unit target) {
        weapon.Attack(target);
        UnitTurnManager.EndTurn();
    }//Attack

    public void OnDrawGizmos() {
        if (availableTiles != null) {
            for (int i = 0; i < availableTiles.Count; i++) {
                Gizmos.color = Color.blue;
                Gizmos.DrawCube(availableTiles[i], Vector3.one * .9f);
            }
        }

        if (path != null) {
            for (int i = 0; i < path.Length; i++) {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one * .9f);
            }
        }
    }//OnDrawGizmos

}


