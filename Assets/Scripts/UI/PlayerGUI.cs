﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerGUI : MonoBehaviour {

    public Text stateText;
    public Text targetUnitText;

    public void EnterIdleState() {
        UnitTurnManager.SetState(UnitTurnManager.TurnState.Idle);
    }

    public void EnterMoveState() {
        UnitTurnManager.SetState(UnitTurnManager.TurnState.Moving);
        UnitTurnManager.GetMoveTiles();
    }

    public void EnterActionState() {
        UnitTurnManager.SetState(UnitTurnManager.TurnState.Action);
        UnitTurnManager.GetAttackTiles(UnitTurnManager.ControlledUnit.weapon);
    }

	public void EndTurn() {
        UnitTurnManager.EndTurn();
    }

    //void Update() {

    //    //if(UnitTurnManager.TargettedUnit!=null)
    //    //    targetUnitText.text = UnitTurnManager.TargettedUnit.name;
    //    //else
    //    //    targetUnitText.text = "No targetted units";

    //    switch (UnitTurnManager.state) {
    //        case UnitTurnManager.TurnState.Idle:
    //            stateText.text = "State: IDLE";
    //            break;
    //        case UnitTurnManager.TurnState.Moving:
    //            stateText.text = "State: MOVING";
    //            break;
    //        case UnitTurnManager.TurnState.Action:
    //            stateText.text = "State: ACTION";
    //            break;
    //    }

    //}
}
