﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowPlayerTurn : MonoBehaviour {
    static ShowPlayerTurn instance;

    public bool active = false;
    public float scrollSpeed;
    public Color[]teamColours;
    Image[] bar;
    Text text;
    public int team;

    void Awake(){
        instance = this;
    }

    void Start () {
        bar = new Image[3];

        bar[0] = transform.FindChild("TopBar").GetComponent<Image>();
        bar[1] = transform.FindChild("BottomBar").GetComponent<Image>();
        bar[2] = transform.FindChild("MiddleBar").GetComponent<Image>();

        text = transform.FindChild("MainText").GetComponent<Text>();

        transform.position = new Vector3(-Screen.width, Screen.height / 2, 0);
        SetTeamColours();
	}

    /*
    Determine which team is being representated using a boolean and running the 
    Show coroutine.
    Input is disabled while the screen is being shown.
    */
    public static void DisplayTurnChange(bool playerTurn){
        int _team = 0;
        if (!playerTurn)
            _team = 1;

        instance.team = _team;
        instance.SetTeamColours();
        GameController.InputEnabled = false;
        instance.StartCoroutine("Show", instance.scrollSpeed);
    }

    /*
    Change the colour of the bars depending on the team being shown.
    */
    void SetTeamColours(){
        bar[0].color = teamColours[team + 2];
        bar[1].color = teamColours[team + 2];
        bar[2].color = teamColours[team];

        if (team == 0)
            text.text = "Player's Turn";
        else
            if (team == 1)
            text.text = "Enemy's Turn";

    }
    /*
    Coroutine to have the screen scroll in from the left side, stay for a few seconds and scroll
    off screen.
    WARNGING: Coroutine breaks when game window is at certain resolutions
    - NEEDS FIXING!
    */
	IEnumerator Show(float time){
        active = true;
        Vector3 onScreen = new Vector3(0, Mathf.Round(Screen.height / 2), 0);
        Vector3 endPos = new Vector3(Screen.width, Screen.height / 2, 0);
        
        while(transform.position != onScreen){
            transform.position = Vector3.MoveTowards(transform.position, onScreen, time * Time.deltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(1);

        while (active){
            if (transform.position == endPos)
                active = false;
            transform.position = Vector3.MoveTowards(transform.position, endPos, time * Time.deltaTime);
            yield return null;
        }
        
        //active = false;
        Reset();
    }//Show

    /*
    Reset the position of the canvas to the left edge of the game screen, and re-enable
    player input.
    */
    void Reset(){
        transform.position = new Vector3(-Screen.width, Screen.height / 2, 0);
        GameController.InputEnabled = true;
    }
}
