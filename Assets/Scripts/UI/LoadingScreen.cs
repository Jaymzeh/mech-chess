﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreen : MonoBehaviour {

    public static LoadingScreen loadingScreen;

    public GameObject background;
    public GameObject text;
    public GameObject progressbar;

    private int progress = 0;

    /*
    Remove any other LoadingScreen objects in the scene.
    */
    void Awake() {
        if (loadingScreen == null) {
            DontDestroyOnLoad(gameObject);
            loadingScreen = this;
        }
        else if (loadingScreen != this)
            Destroy(gameObject);
    }
    
    /*
    Disable the loading screen when loaded.
    */
    void Start() {
        background.SetActive(false);
        text.SetActive(false);
        progressbar.SetActive(false);
    }
    
    /*
    Load in a new scene.
    */
    public void ChangeScene(string scene) {
        Debug.Log("Loading scene: " + scene);
        StartCoroutine(DisplayLoadingScreen(scene));
    }
    
    /*
    Show the loading screen while the new scene is being loaded into memory.
    */
    IEnumerator DisplayLoadingScreen(string scene) {
        background.SetActive(true);
        text.SetActive(true);
        progressbar.SetActive(true);

        progressbar.transform.localScale = new Vector3(progress, progressbar.transform.localScale.y,
            progressbar.transform.localScale.z);

        text.GetComponent<GUIText>().text = "Loading: " + progress + "%";

        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        while (!async.isDone) {
            progress = (int)(async.progress * 100);
            text.GetComponent<GUIText>().text = "Loading: " + progress + "%";

            progressbar.transform.localScale = new Vector3(async.progress, progressbar.transform.localScale.y,
            progressbar.transform.localScale.z);

            yield return null;
        }
        //gameControl.AddComponent<PlayerSpawn>();
        background.SetActive(false);
        text.SetActive(false);
        progressbar.SetActive(false);

    }
}
