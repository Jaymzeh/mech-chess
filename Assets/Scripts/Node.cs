﻿using UnityEngine;
using System.Collections;

/*
Node class used for pathfinding.
*/
public class Node : IHeapItem<Node>{
    public bool walkable;
    public bool occupied;
    public Vector3 worldPosition;
    public int gridX, gridY;

    public int gCost, hCost;
    public Node parent;
    int heapIndex;

    /*
    Create new node with the relevant data.
    Is the node is walkable, is it occupied by a unit, the nodes position.
    */
    public Node(bool _walkable, bool _occupied, Vector3 _worldPos, int _gridX, int _gridY){
        walkable = _walkable;
        occupied = _occupied;
        worldPosition = _worldPos;
        gridX = _gridX;
        gridY = _gridY;
    }//Node

    public int fCost {
        get{
            return gCost + hCost;
        }
    }//fCost

    public int HeapIndex{
        get{
            return heapIndex;
        }
        set{
            heapIndex = value;
        }
    }//HeapIndex

    public int CompareTo(Node node){
        int compare = fCost.CompareTo(node.fCost);
        if (compare == 0){
            compare = hCost.CompareTo(node.hCost);
        }
        return -compare;
    }//CompareTo
}
