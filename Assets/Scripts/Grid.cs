﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class Grid : MonoBehaviour {
    public bool displayGrid;
    public LayerMask unwalkableMask, unitMask;
    public LayerMask gridLayer;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    protected Node[,] grid;

    protected float nodeDiameter;
    protected int gridSizeX, gridSizeY;

    void Awake() {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }//Awake

    public int MaxSize {
        get {
            return gridSizeX * gridSizeY;
        }
    }//MaxSize

    public Node[,] Nodes {
        get {
            return grid;
        }
    }//Nodes

    /*
    Populate the Node array with nodes that can be used with the Pathfinding class.
    Nodes will chck if they are open of blocked by objects int he game world when being created.
    */
    public void CreateGrid() {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++) {
            for (int y = 0; y < gridSizeY; y++) {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);

                //while (!Physics.CheckSphere(worldPoint, nodeRadius, gridLayer)) {
                //    if (worldPoint.y <= -50)
                //        break;

                //    worldPoint += (Vector3.down * (nodeRadius/2));
                //}

                //bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                //bool occupied = Physics.CheckSphere(worldPoint, nodeRadius, unitMask);
                
                grid[x, y] = new Node(false, false, worldPoint, x, y);
            }
        }
    }//CreateGrid

    /*
    Update the grid of nodes after an object has moved to determine if nodes are blocked or open.
    */
    public void UpdateGrid(){
        print("Updating grid");
        for (int x = 0; x < gridSizeX; x++){
            for (int y = 0; y < gridSizeY; y++){
                
                bool walkable = !(Physics.CheckSphere(grid[x,y].worldPosition, nodeRadius, unwalkableMask));
                bool occupied = Physics.CheckSphere(grid[x,y].worldPosition, nodeRadius, unitMask);

                grid[x, y].walkable = walkable;
                grid[x, y].occupied = occupied;
            }
        }
    }//UpdateGrid

    /*
    Return all adjacent tiles.
    */
    public List<Node> GetNeighbours(Node node){
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++){
            for (int y = -1; y <= 1; y++){
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY){
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }
        return neighbours;
    }//GetNeighbours

    /*
    get the node which is closest to the Vector3 position passed in.
    */
    public Node NodeFromWorldPoint(Vector3 worldPosition){
        float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
        return grid[x, y];
    }//NodeFromWorldPoint

    void OnDrawGizmos(){
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null && displayGrid){
            foreach (Node n in grid){
                Gizmos.color = (n.walkable) ? Color.white : Color.red;
                if (n.occupied)
                    Gizmos.color = Color.blue;
                Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - .9f));
            }
        }
    }//OnDrawGizmos
}