﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Mouse : MonoBehaviour {

    public Grid grid;
    public GameObject target;
    Transform mesh;

    void Start() {
        mesh = GetComponentInChildren<MeshRenderer>().transform;
    }

    void Update() {
        mesh.Rotate(Vector3.up, 1);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            target = hit.collider.gameObject;

            transform.position = grid.NodeFromWorldPoint(hit.point).worldPosition;

            if (GameController.InputEnabled) {
                if (!EventSystem.current.IsPointerOverGameObject()) {

                    if (Input.GetMouseButtonDown(0) && UnitTurnManager.ControlledUnit != null) {

                        if (UnitTurnManager.state == UnitTurnManager.TurnState.Moving) {
                            UnitTurnManager.ControlledUnit.Move(transform.position);
                        }
                        else
                            if (UnitTurnManager.state == UnitTurnManager.TurnState.Action) {
                            //UnitTurnManager.TargettedUnit = grid.NodeFromWorldPoint(transform.position).unit;
                            if (target.GetComponent<Unit>() && target.name != UnitTurnManager.ControlledUnit.name)
                                UnitTurnManager.TargettedUnit = target.GetComponent<Unit>();
                        }
                    }
                    //if (Input.GetMouseButtonDown(1))
                    //    if (target.GetComponent<Unit>())
                    //        UnitTurnManager.TargettedUnit = target.GetComponent<Unit>();
                }
            }
        }
    }//Update
}
