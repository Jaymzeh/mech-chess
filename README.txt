Mech Chess
Jaymzeh on BitBucket.org

******Info******
The project contained in the repository was created to experiment with using A* pathfinding and to assess the possibility of using it for creating a turn-based strategy game within Unity 5.

******KNOWN BUGS******
ShowPlayerTurn.cs
Coroutine goes into a loop when bringing the text on-screen when the screen is at a certain resolution. Script has been disabled to avoid causing further issues.

Grid tiles
When a unit is destroyed the tile is was on stays occupied for one turn.


******Acknowledgments******
Sebastian Lague
Provided the base implementation of the A* pathfinding system. Accessed from youtube.com.

Unit Mesh
Mech 5 Thunderbird and textures created by Skorpio (C) 2011
Accessed from opengameart.org.
Released under 
CC-BY-SA 3.0 
GNU GPL 3